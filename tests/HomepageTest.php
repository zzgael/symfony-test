<?php namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class HomepageTest extends WebTestCase
{
    public function testHome()
    {
        $client = static::createClient();

        $client->request('GET', "/");
        $this->assertSame(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            "The homepage does not loads correctly."
        );
    }

    public function test404()
    {
        $client = static::createClient();

        $client->request('GET', "/badurl");
        $this->assertSame(
            Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode(),
            "The bad url does not respond with 404."
        );
    }
}
